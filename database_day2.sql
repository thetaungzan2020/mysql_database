drop database if exists online_db;

create database online_db;

use online_db;

create table employee(
    emp_id INT NOT NULL,
    first_name VARCHAR(32) NOT NULL,
    last_name VARCHAR(32) NOT NULL,
    age VARCHAR(32) NOT NULL,
    email VARCHAR(32) NOT NULL
);

-- source /home/thetaungzan/Desktop/mysql_lesson/database_day2.sql

-- all list
select * from table_name;
-- one field list
select field_name from table_name;

