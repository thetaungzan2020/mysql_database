-- create database database_name
create database demo_db;
-- delete database database_name
drop database demo_db;
-- list database show
show database;

drop database if exists demo_db;

-- create database online_db;
create database if not exists online_db;

-- ddl(data definition language => create table , drop table , alter table)
-- create table employee(
--     employee_id INT,
--     employee_name VARCHAR(32)
-- );
-- Unsign not (-)
create table person(
    person_id smallint not null,
    first_name VARCHAR(32) not null,
    last_name VARCHAR(32) not null,
    education VARCHAR(255) not null,
    primary key(person_id)
);
-- info table field 
desc person;

-- add coloum field command
ALTER TABLE person ADD age int unsigned not null;
-- remove coloum field command
ALTER TABLE person DROP education;

-- show all command
select * for person;

