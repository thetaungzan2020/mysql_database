create database if not exists dml_db;

use dml_db;

CREATE TABLE student(
    student_id INT NOT NULL AUTO_INCREMENT,
    first_name VARCHAR(32) NOT NULL,
    last_name VARCHAR(32) NOT NULL,
    student_address VARCHAR(32) NOT NULL,
    primary key(student_id)
);

-- rename to table name
ALTER TABLE student RENAME TO person;
-- method 2
RENAME TABLE person TO student;

-- set value for database
INSERT INTO student VALUES(1,'thomas','james','yangon'); 

INSERT INTO student VALUES(2,'doe','jhon','yangon'),
(3,'maung','aung','mandalay'),(4,'kyaw','su','bago');

INSERT INTO student(first_name,last_name,student_address,age) VALUES('moe','aung','yangon',22),
('aung','aung','mandalay',21),('wine','su','bago',32);

ALTER TABLE student ADD age int(3) NOT NULL;

-- read value for database
select * from student;

select student_id,student_address from student;

-- update value for database
UPDATE student SET first_name = 'kathy' where age=22;
UPDATE student SET age = 22 where student_id=3;

-- delete value for database
-- delete from student; not count start 1
truncate table student;

